#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install vim

# pathogen
mkdir -p ~/.vim/autoload ~/.vim/bundle; \
curl -Sso ~/.vim/autoload/pathogen.vim \
    https://raw.github.com/tpope/vim-pathogen/master/autoload/pathogen.vim

# vim-sensible
cd ~/.vim/bundle
git clone git@github.com/tpope/vim-sensible.git

# nerd tree
cd ~/.vim/bundle
git clone git@github.com:/scrooloose/nerdtree.git

# splice
git clone git@github.com:/sjl/splice.vim.git ~/.vim/bundle/splice

# easymotion
git clone git@github.com:Lokaltog/vim-easymotion.git ~/.vim/bundle/easymotion

# vim-markdown
cd ~/.vim/bundle
git clone git@github.com:/plasticboy/vim-markdown.git

# fugitive git
cd ~/.vim/bundle
git clone git@github.com/tpope/vim-fugitive.git

# vim-airline status bar
git clone git@github.com:/bling/vim-airline ~/.vim/bundle/vim-airline

# color schemes
git clone git@github.com:flazz/vim-colorschemes.git ~/.vim/bundle/vim-colorschemes

# airline/powerline fonts
wget https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf
mkdir -p ~/.fonts/ && mv PowerlineSymbols.otf ~/.fonts/
fc-cache -vf ~/.fonts
mkdir -p ~/.config/fontconfig/conf.d/ && mv 10-powerline-symbols.conf ~/.config/fontconfig/conf.d/

# create a .vimrc
cat <<EOF > ~/.vimrc
"load pathogen
execute pathogen#infect()

syntax on
filetype plugin indent on

"color scheme
colorscheme darkburn

"disable folding for markdown
let g:vim_markdown_folding_disabled=1

"show line numbers
set number

"airline/powerline fonts
let g:airline_powerline_fonts = 1
EOF