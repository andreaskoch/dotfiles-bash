#!/usr/bin/env bash
# Contribute from a source-directory to a target-directory.

# Filename:
# contribue-source-to-target.sh

# Usage:
# contribue-source-to-target.sh "<name>" "<source-direcotry-path>" "<target-direcotry-path>"

# Get parameters
projectname="$1"
sourcedirectory="$2"
targetdirectory="$3"

# Validate parameters
# Check if a project name has been specified
if [ -z "$projectname" ]; then
    echo "Please specify a name for the synchronization."
    exit
fi

# Check if a source directory has been specified
if [ -z "$sourcedirectory" ]; then
    echo "You must specifiy a source directory path."
    exit
fi

# Check if a target directory has been specified
if [ -z "$targetdirectory" ]; then
    echo "You must specify a target directory path."
    exit
fi

# Check if the source directory exists
if [ ! -d "$sourcedirectory" ]; then
    echo "The source directory $sourcedirecory does not exist."
    exit
fi

# Check if the target directory exists
if [ ! -d "$targetdirectory" ]; then
    echo "The target directory $targetdirectory does not exist."
    exit
fi

# Assemble log file path
logdirectory="$HOME/logs"
logfilename="contribue-from-source-to-target-${projectname// /-}"
logfilepath="$logdirectory/$logfilename.log"

# Create the log directory if it does not exist
if [ ! -d "$logdirectory" ]; then
    echo "Creating the log file direcotry $logdirectory."
    mkdir -p "$logdirectory"
fi

# Start the contribution
rsync -itru --log-file="$logfilepath" "$sourcedirectory" "$targetdirectory"
